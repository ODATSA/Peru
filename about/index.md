---
layout: default
---

# About

The blog for the Open Design of Appropriate Technology for Sustainable Agriculture project by [Dr. Devin Berg](https://www.devinberg.com) from the University of Wisconsin-Stout. More documents and files related to this project can be found at DOI: [10.17605/OSF.IO/7Q3MA](https://doi.org/10.17605/OSF.IO/7Q3MA).