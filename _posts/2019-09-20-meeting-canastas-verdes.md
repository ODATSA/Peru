---
title: "Meeting Canastas Verdes"
tags: [Perú, organic farming, Urubamba, Canastas Verdes]
category: Urubamba
permalink: 2019/09/meeting-canastas-verdes
post_author: Devin R. Berg
author_url: https://www.devinberg.com
layout: post
comments: true
---



# Canastas Verdes

[Canastas Verdes](https://canastasverdes.com/) is a group of women farmers committed to the principles of organic farming and respect for Pachamama. What started with a workshop on permaculture has developed into a small organization hoping to use sustainable practices and educate their community on the benefits of farming in a more sustainable manner ([read more](http://www.linguistichorizons.com/my-internship-in-peru/)).

![The Canastas Verdes group with Devin Berg.](https://osf.io/6p7cb/download)

# Chakras

The women of Canastas Verdes have six farming plots, called Chakras, located around Urubamba. Five of these are operated by individuals, along with their families, and one is shared between two of the women.

![Small farming plot operated by Canastas Verdes.](https://osf.io/a5hxz/download)

On these plots, the women grow organic vegetables that they use to feed their families, both directly and by selling the excess in the Urubamba market and via delivery to nearby communities. Their primary customers are expats who live in or are visiting the valley and desire fresh, organic produce. Through their practices, the women demonstrate a deep commitment to their work, treating the plants that they cultivate as their own children. When asked how they know when to water their gardens or otherwise care for them, they will tell you that they just know. They are in their gardens every day and can see day-to-day what their plants need. They've learned from the knowledge passed down by their ancestors and from their own experiences.

