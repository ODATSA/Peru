---
title: "Introduction to ODATSA"
tags: [meta]
category: Meta
permalink: 2019/09/introduction
post_author: Devin R. Berg
author_url: https://www.devinberg.com
layout: post
comments: true
---



# What is ODATSA?

For the 2019/2020 academic year I was fortunate to be awarded a full year sabbatical from the University of Wisconsin-Stout. The sabbatical affords me the opportunity to focus my attention on a particular research project. I went into this opportunity with the goal of exploring a slightly new area for myself, drawing upon my experience working with Engineers Without Borders USA and other similar work I've participated in over the past few years. With this in mind, I wanted to bridge my interests in smart technology and automation with my interests in sustainable food systems. Therefore, the idea of doing some work in sustainable agriculture from an appropriate technology perspective came to mind. Finally, as the director of [Open Engineering](https://www.openengr.com/) and [a general proponent of doing engineering work in an open, accessible manner](https://f1000research.com/articles/7-501), I wanted to incorporate the concept of open design into this project. Thus the name ODATSA = **O**pen **D**esign of **A**ppropriate **T**echnology for **S**ustainable **A**griculture.

# The first phase

Being on a sabbatical afforded me the opportunity have a lot of flexibility in how and where I work. Through some of my colleagues at Stout, I had made a few connections in Perú. This seemed like a great opportunity to try to strengthen those connections as Perú is a beautiful country with a rich agricultural history. Specifically I was interested in the Andean region of Perú where there exists great biodiversity (paricularly in their potato crops) as well as great challenges such as water security and a vulnerability to climate change.

# What's next?

As with any appropriate engineering design project, I'm spending the first while learning. I'm getting to know the people in my temporary home in Urubamba (in the heart of Perú's sacred valley), I'm improving my Spanish language proficiency, and I'm trying to better understand local agricultural practices. In keeping with philosophy of [appropriate technology](https://en.wikipedia.org/wiki/Appropriate_technology), it is critical that a design or bit of technology integrates with regional practices and respects local knowledge. There is a lot to learn!

![Mountains views from the ruins of Machu Picchu]({{ site.baseurl }}/images/MP-mountains.jpg)