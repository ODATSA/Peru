---
title: "Delayed plans for a return to Perú"
tags: [Perú, research, planning, COVID19, pandemic]
category: Sacred Valley
permalink: 2020/08/delayed-plans
post_author: Devin R. Berg
author_url: https://www.devinberg.com
layout: post
comments: true
---

# And then a pandemic happened

When I left Perú at the end of 2019, the plan was to return the following August with students from UW-Stout so that we could continue the work with [Canastas Verdes](https://odatsa.gitlab.io/Peru/2019/09/meeting-canastas-verdes). The idea was that we would use the time between December and August to work on designs for a water catchment and storage system that would work at the small-scale on each of the chakras that the women operate.

Unfortunately, the COVID19 pandemic has put a damper on these plans as the university has restricted all travel until further notice and Perú remains closed to travel. While this has been a disappointing setback, I hope that the situation will improve with enough time and that we will be able to resume our efforts to provide a sustainable water solution for the women of Canastas Verdes.