---
title: "Learning About Potatoes"
tags: [Perú, potatoes, biodiversity, Chinchero]
category: Chinchero
permalink: 2019/09/learning-about-potatoes
post_author: Devin R. Berg
author_url: https://www.devinberg.com
layout: post
comments: true
---



# Potatoes in Chinchero

Salustio and his family grow potatoes in Chinchero at about 12,500 feet in elevation. They grow 350+ varieties of potatoes, some of which were first cultivated by the Inca and are rarely found being grown in Perú today. His family is very interested in continuing to cultivate and preserve these ancient vareties and are afraid that the biodiversity of Peruvian potatoes will eventually be lost. 

![A close-up photo of a Peruvian potato with a diversity of potato varieties in the background.](https://osf.io/vc93x/download)

The potatoes that they grow have a variety of uses. Some are grown for more mass-market consumption such as in regional restaurants. Others are grown for their health benefits, such as having curative or preventative properties.

![A close-up photo of a couple of Peruvian potato varieties cut open to show the unique colors inside.](https://osf.io/4b5ym/download)

Salustio's family is fully invested in the potato culture. They consume their own potatoes daily, regularly featured in their daily meals as both a main item as well as using sweet varieties for their desert.

The traditional method for cooking potatoes in the highlands is using a huatia or earthen oven where the potatoes are buried with hot coals and left to cook for hours before being dug up and prepared for consumption.

<figure class="video_container">
  <iframe src="https://mfr.osf.io/render?url=https://osf.io/ybtw6/?action=download%26mode=render" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
