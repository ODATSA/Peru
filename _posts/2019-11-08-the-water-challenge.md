---
title: "The Water Management Challenge"
tags: [Perú, water, Canastas Verdes, Urubamba, climate change]
category: Urubamba
permalink: 2019/11/water-management-challenge
post_author: Devin R. Berg
author_url: https://www.devinberg.com
layout: post
comments: true
---



# Water usage for agriculture

During the dry season, the current methods for watering of fields and farms consist primarily of using glacial melt water routed through open channels. This [video on Facebook](https://www.facebook.com/watch/?v=573575143400470) from Municipalidad Provincial de Urubamba provides a brief overview on how some of the water canals are used to distribute water. Access to this water is purchased in allotments of time rather than by volume. This often leads to overwatering and inappropriate management of water resources. The next photograph shows a field being irrigated. Parts of the field are receiving too much water while other parts receive little. This can lead to issues such as root rot and crops which are susceptible to wind damage.

![Image of a field being irrigated showing substantial flooding.](https://osf.io/mhcjs/download)

Often, this overwatering causes excessive runoff. The photograph below shows a substantial volume of water draining out of a field and running into the nearby roadway.

![Irrigation water leaking from a field into a nearby roadway.](https://osf.io/gmnkp/download)

This overwatering is likely due to a number of factors. However, a significant contributor is the unpredictability of water access, which can lead some farmers make use of as much water as possible when they do have access, regardless of need. In general, many farmers do not have a strong sense of the daily water needs of their crops and even those that do, are unable to make the appropriate water allotments, again due to the unpredictability of water access.

# Managing water usage and access

Reliance on glacial melt for dry season water access is risky as recently published research has found that Peruvian glaciers have receded by about 30% between the years 2000 and 2016 {% cite SeehausChangestropicalglaciers2019 %}. The report is based on data {% cite SeehausSurfaceelevationchanges2019 %} which examined several glacial regions across Perú, including those that serve the Sacred Valley and regions surrounding Urubamba. These findings confirm earlier reports on the vulnerability of the glaciers to climate change {% cite Kaserimpactglaciersrunoff2003 RabatelCurrentstateglaciers2013a %} and a continuation of previously reported recession {% cite Georges20thCenturyGlacierFluctuations2004 %}.

One compounding issue is that the infrastructure for managing water in times of surplus are limited. With little in the way of reservoirs that could be drawn from in times of need, the excess water instead flows to the river.

![High volume of water flowing down the central drainage canal.](https://osf.io/e79j6/download)

In terms of water management at each farm, it may be possible to perform calculations for the [evapotranspiration](https://en.wikipedia.org/wiki/Evapotranspiration) rates for a given farm and the farm's products. It can be difficult to develop accurate numbers, but if obtained, then these rates could be used to estimate the more precise water needs of each plot. Allowing for more controlled management of glacial runoff and water rationing according to the needs of the crops.

These are two issues we are hoping to tackle through this project: 1) determining water needs for each chakra based on plot area and products grown, 2) developing a miniature scale reservoir at the chakra for rainwater harvesting to support controlled water rationing during the dry season, in times that the canal water is not available.

## References
{% bibliography --cited %}