---
title: "Making Biol"
tags: [Perú, organic farming, Canastas Verdes, Urubamba, compost]
category: Urubamba
permalink: 2019/10/making-biol
post_author: Devin R. Berg
author_url: https://www.devinberg.com
layout: post
comments: true
---



# Making biol

We were lucky to have the women from Canastas Verdes invite us to a work night where they would be making biol, a form of organic compost.

The recipe used to make the biol is passed down through generations and refined through trial and error. Hopefully arriving at a solution that works for the local land and is adapted for changing climate conditions. The recipe we worked on included fresh greens, manure, old bananas, water, milk, and a few other mix-ins.

![A collection of ingredients such as manure that are collected in buckets and used to make biol.](https://osf.io/u52py/download)

All of the ingredients were added into a large plastic bag where they will sit for the next three months to ferment. The bag had to be massaged to mix everything together and a vent tube was added to deal with the gas that will be produced by the fermentation process.

![A completed biol bag ready to ferment for three months.](https://osf.io/ykfjm/download)

# A community gathering

The best part about the whole process was that it was really a community event. The Canastas Verdes women come together to do the work and their families often come along to help. This helps for sitations when the work would be too difficult for a singler person to do and it creates more of a collective atmosphere where they can help each other. After the work is done, the host serves food, sometimes sweets and coffee or tea, sometimes potatoes and cheese.