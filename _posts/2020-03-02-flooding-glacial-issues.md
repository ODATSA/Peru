---
title: "Flooding and glacial issues in the Sacred Valley"
tags: [Perú, water, Sacred Valley, climate change, Machu Picchu]
category: Sacred Valley
permalink: 2020/03/flooding-glacial-issues
post_author: Devin R. Berg
author_url: https://www.devinberg.com
layout: post
comments: true
---

# Recent failure at Salcantay

The vulnerability of the Sacred Valley has been on display recently as a collapse at the Salcantay glacier has resulted in flooding of the Salcantay River and multiple deaths and injuries in the towns on the river. It [was estimated](https://blogs.agu.org/landslideblog/2020/02/27/salkantay-1/) that 14 million ft<sup>3</sup> broke off and resulted in the drastic flooding. [An analysis of the failure](https://watchers.news/2020/02/27/massive-glacier-collapse-and-catastrophic-mudflow-near-machu-picchu-peru/) suggested that the debris would have included both rock and ice.

<iframe width="560" height="315" src="https://www.youtube.com/embed/mgWENCSw4iI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# The Urubamba River has also been flooding

It was just a few weeks ago that the [Urubamba River was flooding](https://www.facebook.com/Alh5549717/posts/2641604559294642) from heavy rains that have been hitting the area this Spring. Photos show [the damage done in Pisac](https://www.facebook.com/groups/165019380222804/permalink/2880709691987079/) from this flooding. Some were [forced from their homes in Pisac](https://www.facebook.com/tim.butterly/posts/10222707448260386) and the rains have delayed the start of the school year for some as well. Some recent videos [[1](https://www.facebook.com/Alh5549717/videos/2641603682628063)][[2](https://www.facebook.com/Alh5549717/videos/2641603572628074)] have shown some of the extent of the flooding.

These issues speak to the extremes of the Sacred Valley, involving a dry season that limits farmer's ability to grow crops and forces the use of pesticides and other means of soil disruption to maintain their livelihood, followed by the rainy season which produces issues described here and floods crop lands leading to crop loss and other disasters.