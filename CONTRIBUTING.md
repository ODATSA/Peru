# How to submit to the ODATSA Peru blog

The process for submitting to the blog is relatively straightforward.
1. Have a recently published work that you would like to write about.
2. Download the [template](https://gitlab.com/ODATSA/Peru/raw/master/_drafts/template.md).
3. Use the template to fill in the details about you and your post in [Markdown](https://guides.github.com/features/mastering-markdown/).
4. Fork the [ODATSA Peru repo](https://gitlab.com/ODATSA/Peru).*
5. Save your authored post to the `_posts` folder using the `YYYY-MM-URL-title-goes-here.md` file naming format.
6. Add any images to the `/images` folder with a file name such as `YYYY-MM-URL-YourName-filename.png`.
7. [Submit a merge request](https://gitlab.com/ODATSA/Peru/merge_requests) with your additions.
8. If any corrections are needed prior to posting, you will be notified via a comment on your pull request.

\* Alternatively you can navigate to the [_posts](https://gitlab.com/ODATSA/Peru/tree/master/_posts) folder in the Gitlab repo and click create or upload a file to simultaneously create a fork and submit a merge request.
